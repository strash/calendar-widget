const MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
const WEEKDAYS = [ 'вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб' ];


class CalendarDate {
  constructor(container) {
    this.self = container;
    this.active = false;
  }

  cleanTimestamp(value) {
    return new Date(value).setHours(0, 0, 0, 0);
  }

  // таймстемп
  set value(value) {
    this.$value = this.cleanTimestamp(value);
    this.date = new Date(this.$value).getDate();
    this.self.textContent = this.date;
  }

  get value() {
    return this.$value;
  }

  // проверка на соответствие ноды
  isSelf(node) {
    return this.self.isSameNode(node);
  }

  // установка статуса
  set active(status) {
    this.$active = status;
    if (status) this.self.classList.add('active');
    else this.self.classList.remove('active');
  }

  get active() {
    return this.$active;
  }
}


// МЕСЯЦ И ГОД
function renderSelects(months, timestamp) {
  const controls           = document.createElement('div');
        controls.className = 'calendar-widget-control-select';
  this.container.appendChild(controls);

  const month = document.createElement('select');
  const year  = document.createElement('select');
  controls.appendChild(month);
  controls.appendChild(year);

  const currentMonth = timestamp ? new Date(timestamp).getMonth() : new Date().getMonth();
  months.forEach((v, i) => {
    const option                              = document.createElement('option');
          option.value                        = v;
          option.textContent                  = v;
    if    (i == currentMonth) option.selected = true;
    month.appendChild(option);
  });

  const currentYear = timestamp ? new Date(timestamp).getFullYear() : new Date().getFullYear();
  const actualYear  = new Date().getFullYear();
  let   i           = currentYear - this.yearRange;
  while (i < currentYear + this.yearRange + 1 || i < actualYear + this.yearRange + 1 && currentYear < actualYear) {
    const option                             = document.createElement('option');
          option.value                       = i;
          option.textContent                 = i;
    if    (i == currentYear) option.selected = true;
    year.appendChild(option);
    i++;
  }

  return { month, year };
}


// ТЕКСТ ЛИСТАЛКИ
function setPage(months, timestamp) {
  return timestamp ? `${months[new Date(timestamp).getMonth()]} ${new Date(timestamp).getFullYear()}` : `${months[new Date().getMonth()]} ${new Date().getFullYear()}`;
}


// ЛИСТАЛКА
function renderPaging(months, timestamp) {
  const controls           = document.createElement('div');
        controls.className = 'calendar-widget-control-paging';
  this.container.appendChild(controls);

  const leftPage            = document.createElement('span');
  const page                = document.createElement('p');
  const rightPage           = document.createElement('span');

  controls.appendChild(leftPage);
  controls.appendChild(page);
  controls.appendChild(rightPage);

  leftPage.textContent = '<';
  page.textContent = setPage(months, timestamp);
  rightPage.textContent = '>';

  return { leftPage, page, rightPage };
}

// СЕТКА С ДАТАМИ И ДНЯМИ НЕДЕЛИ
function renderDates(weekDays) {
  const grid = document.createElement('div');
  grid.setAttribute('style', ` display: grid; grid-template-columns: repeat(7, auto); align-items: center; text-align: right; position: relative;`);
  grid.className = 'calendar-widget-grid-container';

  // установка дней недели
  weekDays.forEach((v, i) => {
    const weekDay = document.createElement('span');
    weekDay.className = 'calendar-widget-weekday';
    if (i == 0 || i > 5) weekDay.classList.add('calendar-widget-weekday-weekend');
    weekDay.textContent = v;
    if (!this.isSundayFirst && i == 0) weekDay.style.gridColumn = '7 / 8';
    weekDay.style.gridRow = '1 / 2';
    grid.appendChild(weekDay);
  });

  // установка дат текущего месяца
  for (let i = 0; i < 42; i++) {
    const date = document.createElement('span');
    date.className = 'calendar-widget-date';
    grid.appendChild(date);
    this.dates.push(new CalendarDate(date));
  }

  this.container.appendChild(grid);
  return grid;
}

class CalendarWidget {
  constructor({
    container     = document.body,                   // контейнер
    weekDays      = WEEKDAYS,                        // отображаемые дни недели
    months        = MONTHS,                          // отображаемые месяцы
    isSundayFirst = false,                           // установка начала недели
    yearRange     = 10,                              // количество лет перед и после текущего года
    activeDate    = null,                            // активная дата по-умолчанию
    controls      = 'all' || 'select' || 'paging',   // 'all' - select, paging; 'select'; 'paging'
    callback                                         // колбэк
  }) {
    this.container        = container;
    this.weekDays         = weekDays;
    this.months           = months;
    this.isSundayFirst    = isSundayFirst;
    this.yearRange        = yearRange;
    this.activeDate       = activeDate ? new Date(activeDate).setHours(0,0,0,0) : null;
    this.controls         = controls;
    this.callback         = callback;
    this.dates            = [];
    this.activeDateObj    = null;

    let controlsSelect, controlsPaging;
    if (this.controls == 'all' || this.controls == 'select') {
      controlsSelect = renderSelects.call(this, this.months, this.activeDate);
      this.montsSelect = controlsSelect.month;
      this.yearSelect  = controlsSelect.year;
    }
    if (this.controls == 'all' || this.controls == 'paging') {
      controlsPaging = renderPaging.call(this, this.months, this.activeDate);
      this.leftPage  = controlsPaging.leftPage;
      this.page      = controlsPaging.page;
      this.rightPage = controlsPaging.rightPage;
    }

    if (this.controls == 'all' || this.controls == 'select') {
      // выбор месяца
      this.montsSelect.addEventListener('change', this.setSelect.bind(this));
      // выбор года
      this.yearSelect.addEventListener('change', this.setSelect.bind(this));
    }
    if (this.controls == 'all' || this.controls == 'paging') {
      // пейджинг
      this.leftPage.addEventListener('click', this.setPagePrevious.bind(this));
      this.page.addEventListener('click', this.load.bind(this));
      this.rightPage.addEventListener('click', this.setPageNext.bind(this));
    }

    // клик по дате
    const gridDates = renderDates.call(this, weekDays);
    gridDates.addEventListener('click', ({ target }) => {
      if (!target.classList.contains('calendar-widget-date')) return;
      const date = this.dates.find(v => v.isSelf(target));
      date.active = true;
      if (this.activeDateObj && this.activeDateObj.value !== date.value) this.activeDateObj.active = false;
      this.activeDateObj = date;
      this.activeDate = date.value;
      if (this.callback) this.callback(this.getData());
    });

    this.load();
  }

  // текущий таймстемп вьюхи
  get currentTimestamp() {
    return this.$currentTimestamp;
  }
  set currentTimestamp(value) {
    this.$currentTimestamp = value;
    this.setDates(value);

    if (this.controls == 'all' || this.controls == 'select') {
      this.montsSelect.children[new Date(value).getMonth()].selected = true;
      for (let i = 0; i < this.yearSelect.children.length; i++) {
        if (+this.yearSelect.children[i].value == new Date(value).getFullYear()) {
          this.yearSelect.children[i].selected = true;
          break;
        }
      }
    }

    if (this.controls == 'all' || this.controls == 'paging') this.page.textContent = setPage(this.months, value);
  }

  // установка даты
  getStampByDate(timestamp, date) {
    return new Date(timestamp).setDate(date);
  }

  // день недели первого числа текущего месяца
  getOffset(timestamp) {
    let day = new Date(timestamp).getDay();
    let offset = 0;
    if (!this.isSundayFirst) {
      if (day == 0) offset = 5;
      else offset = day - 2;
    } else offset = day - 1;
    return -offset;
  }

  // установка селекта
  setSelect() {
    this.currentTimestamp = new Date(+this.yearSelect.value, this.months.findIndex(v => v == this.montsSelect.value));
  }

  // установка предыдущей страницы
  setPagePrevious() {
    const month = new Date(this.currentTimestamp).getMonth() - 1;
    this.currentTimestamp = new Date(this.currentTimestamp).setMonth(month);
  }

  // установка следующей страницы
  setPageNext() {
    const month = new Date(this.currentTimestamp).getMonth() + 1;
    this.currentTimestamp = new Date(this.currentTimestamp).setMonth(month);
  }

  // установка дат
  setDates(timestamp) {
    this.dates.forEach((v, i) => {
      const offset = this.getOffset(this.getStampByDate(timestamp, 1)) + i;
      v.value = this.getStampByDate(timestamp, offset);
      v.self.classList.remove('calendar-widget-date-neighbor');
      if (new Date(timestamp).getMonth() !== new Date(v.value).getMonth()) v.self.classList.add('calendar-widget-date-neighbor');
      if (this.isSundayFirst && i % 7 == 0 || i % 7 == 6 || !this.isSundayFirst && i % 7 == 5) v.self.classList.add('calendar-widget-date-weekend');
      if (new Date().setHours(0,0,0,0) == v.value) v.self.classList.add('calendar-widget-date-today');
      else v.self.classList.remove('calendar-widget-date-today');
      if (this.activeDate && this.activeDate == v.value) {
        v.active = true;
        this.activeDateObj = v;
      } else v.active = false;
    });
  }

  // установка дефолтовой вьюхи
  load() {
    this.currentTimestamp = this.activeDate ? this.activeDate : new Date();
  }

  getData() {
    if (this.activeDateObj) {
      const date = new Date(this.activeDateObj.value);
      return {
        timestamp:   this.activeDateObj.value,
        date:        date.getDate(),
        day:         date.getDay(),
        dayString:   this.weekDays[date.getDay()],
        month:       date.getMonth(),
        monthString: this.months[date.getMonth()],
        year:        date.getFullYear(),
      };
    } else {
      console.warn('Не выбрана дата.');
      return null;
    }
  }
}

export default CalendarWidget;